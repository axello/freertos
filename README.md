# FreeRTOS

These are the examples and challenges belonging to Digi-Key's "Introduction to FreeRTOS" series.

https://www.youtube.com/playlist?list=PLEBQazB0HUyQ4hAPU1cJED6t3DU0h34bz

I don't use the Adafruit Feather, but an ESP32 with OLED display, like this one. I just happened to have that one available.
https://www.banggood.com/Geekcreit-ESP32-OLED-Module-ESP32-OLED-WiFi-Module-+-bluetooth-Dual-ESP-32-p-1181297.html