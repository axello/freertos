#include "SSD1306.h" // alias for `#include "SSD1306Wire.h"`
#include "Stream.h"

SSD1306 display(0x3c, 5, 4);

// Use only 1 core for demo purposes
#if CONFIG_FREERTOS_UNICORE
static const BaseType_t app_cpu = 0;
#else
static const BaseType_t app_cpu = 1;
#endif

static TaskHandle_t task_1 = NULL;
static TaskHandle_t task_2 = NULL;
static int signaal = 0;
static char *transferBuffer = NULL;

const size_t inputLength = 100;

// Read the serial port until newline, then store the string on the heap
// and signal using a semaphore (which we don't know yet, officially)
void taskReadSerial(void *parameter)
{
  char *input;
  input = (char *)malloc(inputLength);

  while (1)
  {
    size_t maxLength;
 
    if (Serial.available())
    {
      maxLength = Serial.readBytesUntil('\n', input, inputLength);

      transferBuffer = (char *)pvPortMalloc((maxLength + 2) * sizeof(char));
      if (transferBuffer != NULL) {
        strncpy(transferBuffer, input, maxLength);
        transferBuffer[maxLength + 1] = 0;
        displayMessage(transferBuffer);
        signaal = 1;      // should be more atomic, but that's next lesson!
      }
    }
    vTaskDelay(25 / portTICK_PERIOD_MS);
  }
}

void taskWriteSerial(void *parameter) {
  while (1) {
    if (1 == signaal && transferBuffer != NULL) {
      Serial.println(transferBuffer);
      Serial.print("Heap before malloc (bytes): ");
      Serial.println(xPortGetFreeHeapSize());
      vPortFree(transferBuffer);
      transferBuffer = NULL;
      signaal = 0;
    }
    vTaskDelay(100 / portTICK_PERIOD_MS);
  }
}

void displayMessage(char *message) {
  display.clear();
  // header
  display.setFont(ArialMT_Plain_16);
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.drawString(0, 5, "Memory Challenge 4");

  display.setFont(ArialMT_Plain_10);
  display.drawString(0, 25, message);
  display.display();
}

void setup() {
  // put your setup code here, to run once:

  Serial.begin(115200);

  display.init();

  // Serial stuff
  vTaskDelay(1000 / portTICK_PERIOD_MS);
  Serial.println();
  Serial.println("---FreeRTOS Memory Challenge---");


  // Task to run forever
  xTaskCreatePinnedToCore(
      taskWriteSerial,
      "Serial Write task",
      1524,
      NULL,
      1,
      &task_1,
      app_cpu);

  xTaskCreatePinnedToCore(
      taskReadSerial,
      "Serial Read task",
      1504,
      NULL,
      2,
      &task_2,
      app_cpu);

  // Delete "setup and loop" task
  vTaskDelete(NULL);
}

void loop() {
}
