// This was a quick hack to find the GPIO pins corresponding to the Arduino pins on the
// Wemos Lolin32 board with OLED display
// In the end, only Pin 16 and 26 were supported.



#include "SSD1306.h" // alias for `#include "SSD1306Wire.h"`
 
SSD1306 display(0x3c, 5, 4);

// Use only 1 core for demo purposes
#if CONFIG_FREERTOS_UNICORE
static const BaseType_t app_cpu = 0;
#else
static const BaseType_t app_cpu = 1;
#endif

// Pins
static const int ledPin = D2;

// our task: blink led
void toggleLED(void *parameter) {
  while(1) {
    digitalWrite(ledPin, HIGH);
    vTaskDelay(500 / portTICK_PERIOD_MS);
    digitalWrite(ledPin, LOW);
    vTaskDelay(500 / portTICK_PERIOD_MS);
    
  }
}

void setup() {
  // put your setup code here, to run once:
  pinMode(ledPin, OUTPUT);
  pinMode(D0, OUTPUT);
    pinMode(D1, OUTPUT);
  pinMode(D2, OUTPUT);
  pinMode(D3, OUTPUT);
    pinMode(D4, OUTPUT);
  pinMode(D5, OUTPUT);

  pinMode(D6, OUTPUT);
  pinMode(D7, OUTPUT);
  pinMode(D8, OUTPUT);

display.init();
display.setFont(ArialMT_Plain_16
);
 display.setTextAlignment(TEXT_ALIGN_LEFT);
  // Task to run forever
//  xTaskCreatePinnedToCore(
//      toggleLED,
//      "Toggle LED",
//      1024,
//      NULL,
//      1,
//      NULL,
//      app_cpu);
      
}

void loop() {
  // put your main code here, to run repeatedly:
   while(1) {
    display.clear();
    display.drawString(0, 10, "LED D0");
    display.display();

    digitalWrite(D0, HIGH);
    delay(500 / portTICK_PERIOD_MS);
    digitalWrite(D0, LOW);
    delay(300 / portTICK_PERIOD_MS);

display.clear();
display.drawString(0, 10, "LED D1");
       display.display();
 
    digitalWrite(D1, HIGH);
    delay(500 / portTICK_PERIOD_MS);
    digitalWrite(D1, LOW);
    delay(300 / portTICK_PERIOD_MS);

display.clear();
display.drawString(0, 10, "LED D2");
       display.display();

    digitalWrite(D2, HIGH);
    delay(500 / portTICK_PERIOD_MS);
    digitalWrite(D2, LOW);
    delay(300 / portTICK_PERIOD_MS);


           display.clear();
           display.drawString(0, 10, "LED D3");
           display.display();
 
    digitalWrite(D3, HIGH);
    delay(500 / portTICK_PERIOD_MS);
    digitalWrite(D3, LOW);
    delay(300 / portTICK_PERIOD_MS);

display.clear();
    display.drawString(0, 10, "LED D4");
           display.display();

    digitalWrite(D4, HIGH);
    delay(500 / portTICK_PERIOD_MS);
    digitalWrite(D4, LOW);
    delay(300 / portTICK_PERIOD_MS);

 display.clear();
 display.drawString(0, 10, "LED D5");
       display.display();
 
    digitalWrite(D5, HIGH);
    delay(500 / portTICK_PERIOD_MS);
    digitalWrite(D5, LOW);
    delay(300 / portTICK_PERIOD_MS);

display.clear();
    display.drawString(0, 10, "LED D6");
     display.display();
    digitalWrite(D6, HIGH);
    delay(500 / portTICK_PERIOD_MS);
    digitalWrite(D6, LOW);
    delay(300 / portTICK_PERIOD_MS);

 display.clear();
 display.drawString(0, 10, "LED D7");
    display.display();
     digitalWrite(D7, HIGH);
    delay(500 / portTICK_PERIOD_MS);
    digitalWrite(D7, LOW);
    delay(300 / portTICK_PERIOD_MS);

display.clear();
display.drawString(0, 10, "LED D8");
    display.display();
 
    digitalWrite(D8, HIGH);
    delay(500 / portTICK_PERIOD_MS);
    digitalWrite(D8, LOW);
    delay(300 / portTICK_PERIOD_MS);

}}
