#include "SSD1306.h" // alias for `#include "SSD1306Wire.h"`

SSD1306 display(0x3c, 5, 4);

// Use only 1 core for demo purposes
#if CONFIG_FREERTOS_UNICORE
static const BaseType_t app_cpu = 0;
#else
static const BaseType_t app_cpu = 1;
#endif

// Pins
static const int ledPin = D4;

// our task: blink led
void toggleLEDOne(void *parameter) {
  while (1) {
    digitalWrite(ledPin, HIGH);
    display.drawString(0, 10, "ONE");
    display.display();
    
    vTaskDelay(212 / portTICK_PERIOD_MS);
    digitalWrite(ledPin, LOW);
    display.drawString(0, 10, "    ");
    display.display();
    vTaskDelay(487 / portTICK_PERIOD_MS);

  }
}

void toggleLEDTwo(void *parameter) {
  while (1) {
    digitalWrite(ledPin, HIGH);
        display.drawString(0, 24, "TWO");
    display.display();

    vTaskDelay(333 / portTICK_PERIOD_MS);
    digitalWrite(ledPin, LOW);
    display.drawString(0, 24, "    ");
    display.display();
    vTaskDelay(205 / portTICK_PERIOD_MS);

  }
}

void setup() {
  // put your setup code here, to run once:
  pinMode(ledPin, OUTPUT);

  display.init();
  display.setFont(ArialMT_Plain_16);
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.clear();
  display.drawString(0, 10, "Task Demo 1");
  display.display();
  // Task to run forever
  xTaskCreatePinnedToCore(
    toggleLEDOne,
    "Toggle LED",
    1024,
    NULL,
    1,
    NULL,
    app_cpu);

  xTaskCreatePinnedToCore(
    toggleLEDTwo,
    "Toggle LED",
    1024,
    NULL,
    1,
    NULL,
    app_cpu);

}

void loop() {
  // put your main code here, to run repeatedly:
}
