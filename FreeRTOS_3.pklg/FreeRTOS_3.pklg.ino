#include "SSD1306.h" // alias for `#include "SSD1306Wire.h"`
#include "Stream.h"

SSD1306 display(0x3c, 5, 4);

// Use only 1 core for demo purposes
#if CONFIG_FREERTOS_UNICORE
static const BaseType_t app_cpu = 0;
#else
static const BaseType_t app_cpu = 1;
#endif

static TaskHandle_t task_1 = NULL;
static TaskHandle_t task_2 = NULL;

const size_t inputLength = 15;

// Variables
int waitTimeOn = 500;
int waitTimeOff = 500;

// Pins
static const int ledPin = D4;

// our task: print string one char at a time
void taskBlinkLED(void *parameter) {
  while (1) {
    if (waitTimeOn > 0 && waitTimeOff > 0) {
      digitalWrite(ledPin, HIGH);
      vTaskDelay(waitTimeOn / portTICK_PERIOD_MS);
      digitalWrite(ledPin, LOW);
      vTaskDelay(waitTimeOff / portTICK_PERIOD_MS);
    }
  }
}

void startTask2(void *parameter) {
  while (1) {
    Serial.print('*');
    vTaskDelay(100 / portTICK_PERIOD_MS);
  }
}


void taskReadSerial(void *parameter) {
  char *input;
  input = (char *) malloc(inputLength);
  
  while (1) {
    size_t maxLength;
    int newOnTime = 100;
    int newOffTime = 100;

    if (Serial.available()) {
      maxLength = Serial.readBytesUntil('\n', input, inputLength);

      // determine if there is a space in the inputstring
      newOnTime = atoi(input);
      Serial.print("New delay is/are: ");
      Serial.print(newOnTime);
      Serial.print(" ");

      char *comma = strtok(input, " ");

      if (comma != NULL) {    // there is a space
        newOffTime = atoi(comma);
        comma = strtok(NULL, " ");
        newOffTime = atoi(comma);
      } else {
        newOffTime = newOnTime;        
      }

      Serial.print("New delay is/are: ");
      Serial.print(newOnTime);
      Serial.print(" ");
      Serial.println(newOffTime);

      waitTimeOn = newOnTime;
      waitTimeOff = newOffTime;
      displayTime(waitTimeOn, waitTimeOff);
    }
    vTaskDelay(25 / portTICK_PERIOD_MS);
  }
}

void displayTime(int aantijd, int uittijd) {
 
  display.clear();
  // header
  display.setFont(ArialMT_Plain_16);
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.drawString(0, 5, "Challenge 3");
  // speed
  display.setFont(ArialMT_Plain_10);
  String message = "on delay ";
  message.concat(aantijd);
  message.concat(" ms");
  display.drawString(0, 25, message);

  message = "off delay ";
  message.concat(uittijd);
  message.concat(" ms");
  display.drawString(0, 37, message);

  display.display();
}

void setup() {
  // put your setup code here, to run once:

  Serial.begin(9600);
  pinMode(ledPin, OUTPUT);

  display.init();
  displayTime(waitTimeOn, waitTimeOff);

  // Serial stuff
  vTaskDelay(1000 / portTICK_PERIOD_MS);
  Serial.println();
  Serial.println("---FreeRTOS Task Demo---");

  Serial.println("Input delay time in ms and press return,");
  Serial.println("or two delays separated by a space, like this: '200 800' and press return.");

  // Task to run forever
  xTaskCreatePinnedToCore(
    taskBlinkLED,
    "Task 1",
    1024,
    NULL,
    1,
    &task_1,
    app_cpu);

  xTaskCreatePinnedToCore(
    taskReadSerial,
    "Task 2",
    1224,
    NULL,
    2,
    &task_2,
    app_cpu);

}

void loop() {
}
